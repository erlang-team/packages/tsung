#### CONFIGURE VARIABLE

# export ERLC_EMULATOR to fix a bug in R9B with native compilation
ERLC_EMULATOR=@ERL@
export ERLC_EMULATOR
ERL=@ERL@
ERLC=@ERLC@
SED=@SED@
ERL_OPTS=@ERL_OPTS@

ERLDIR=@ERLDIR@
export ERLDIR

ERLANG_XMERL_DIR=@ERLANG_XMERL_DIR@

raw_erlang_prefix=@libdir@/erlang/

prefix=$(DESTDIR)@prefix@
exec_prefix=@exec_prefix@
bindir=@bindir@
libdir=@libdir@
datadir=@datadir@
TEMPLATES_SUBDIR=@TEMPLATES_SUBDIR@

CONFIGURE_DEPENDENCIES=@CONFIGURE_DEPENDENCIES@
CONFIG_STATUS_DEPENDENCIES=@CONFIG_STATUS_DEPENDENCIES@

VERSION=@PACKAGE_VERSION@
PACKAGE=@PACKAGE_NAME@
DTD=@DTD@

#### END OF SUBSTITUTION

SVN_REVISION=$Revision: 641 $

ERL_COMPILER_OPTIONS="[warn_unused_vars]"
export ERL_COMPILER_OPTIONS

ifeq ($(TYPE),debug)
OPT =+debug_info -DDEBUG
else 
 ifeq ($(TYPE),native)
   OPT:=+native
  else
   OPT =
  endif	
endif
INC = ./include
CC  = $(ERLC)

ESRC = ./src
EBIN = ./ebin
ifeq ($(TYPE),snapshot)
DAY=$(shell date +"%Y%m%d")
distdir = $(PACKAGE)-$(VERSION)-$(DAY)
else 
distdir = $(PACKAGE)-$(VERSION)
endif

# installation path
BINDIR    = $(bindir)
LIBDIR    = $(libdir)/tsung/bin/
CONFDIR   = $(datadir)/doc/tsung/examples
SHARE_DIR = $(datadir)/tsung/
TEMPLATES_DIR = $(datadir)/$(TEMPLATES_SUBDIR)
MAN_DIR   = $(datadir)/man/man1/
DOC_DIR   = $(datadir)/doc/tsung

ERLANG_LIB_DIR = $(libdir)/erlang/lib

APPLICATION = tsung
CONTROLLER_APPLICATION = tsung_controller
RECORDER_APPLICATION = tsung_recorder

RECORDER_TARGETDIR = $(ERLANG_LIB_DIR)/$(RECORDER_APPLICATION)-$(VERSION)
CONTROLLER_TARGETDIR = $(ERLANG_LIB_DIR)/$(CONTROLLER_APPLICATION)-$(VERSION)
TARGETDIR = $(ERLANG_LIB_DIR)/$(APPLICATION)-$(VERSION)

TEMPLATES = $(wildcard $(ESRC)/templates/*.thtml)
TMP       = $(wildcard *~) $(wildcard src/*~) $(wildcard inc/*~)
INC_FILES = $(wildcard $(INC)/*.hrl)
LIBSRC    = $(wildcard $(ESRC)/lib/*.erl)
SRC       = $(wildcard $(ESRC)/$(APPLICATION)/*.erl)
CONTROLLER_SRC  = $(wildcard $(ESRC)/$(CONTROLLER_APPLICATION)/*.erl)
RECORDER_SRC    = $(wildcard $(ESRC)/$(RECORDER_APPLICATION)/*.erl)
CONFFILE_SRC = $(wildcard examples/*.xml.in)
CONFFILE = $(basename $(CONFFILE_SRC))
USERMANUAL = doc/user_manual.html  doc/IDXDOC.css
USERMANUAL_IMG = $(wildcard doc/images/*.png)
USERMANUAL_SRC = doc/user_manual.tex
PERL_SCRIPTS_SRC = $(wildcard $(ESRC)/*.pl.in)
PERL_SCRIPTS = $(basename $(PERL_SCRIPTS_SRC))

TARGET   = $(addsuffix .beam, $(basename \
             $(addprefix $(EBIN)/, $(notdir $(SRC)))))
LIB_TARGET   = $(addsuffix .beam, $(basename \
             $(addprefix $(EBIN)/, $(notdir $(LIBSRC)))))
CONTROLLER_TARGET   = $(addsuffix .beam, $(basename \
             $(addprefix $(EBIN)/, $(notdir $(CONTROLLER_SRC)))))
RECORDER_TARGET   = $(addsuffix .beam, $(basename \
             $(addprefix $(EBIN)/, $(notdir $(RECORDER_SRC)))))
DEBIAN    = debian/changelog debian/control debian/compat debian/copyright debian/docs debian/tsung.dirs debian/rules

SRC_APPFILES   = $(ESRC)/$(APPLICATION)/$(APPLICATION).app.src $(ESRC)/$(APPLICATION)/$(APPLICATION).rel.src
CONTROLLER_SRC_APPFILES   = $(ESRC)/$(CONTROLLER_APPLICATION)/$(CONTROLLER_APPLICATION).app.src $(ESRC)/$(CONTROLLER_APPLICATION)/$(CONTROLLER_APPLICATION).rel.src
RECORDER_SRC_APPFILES   = $(ESRC)/$(RECORDER_APPLICATION)/$(RECORDER_APPLICATION).app.src $(ESRC)/$(RECORDER_APPLICATION)/$(RECORDER_APPLICATION).rel.src
TGT_APPFILES_E = $(EBIN)/$(APPLICATION).app
CONTROLLER_TGT_APPFILES_E = $(EBIN)/$(CONTROLLER_APPLICATION).app
RECORDER_TGT_APPFILES_E = $(EBIN)/$(RECORDER_APPLICATION).app
TGT_APPFILES_P = priv/$(APPLICATION)*
RECORDER_TGT_APPFILES_P = priv/$(RECORDER_APPLICATION)*
CONTROLLER_TGT_APPFILES_P = priv/$(CONTROLLER_APPLICATION)*

SCRIPT   = $(BINDIR)/tsung
PWD = $(shell pwd)
BUILD_OPTIONS =	'[{systools, \
        [{variables,[ \
         {"TSUNGPATH", "$(PWD)/temp/"}] \
        }]}, \
	    {sh_script, none}, \
        {make_app, true }, {make_rel, true}].'

BUILD_OPTIONS_DOT = $(subst $(PWD)/temp/,./,$(BUILD_OPTIONS))

BUILD_OPTIONS_FILE = ./BUILD_OPTIONS 

DIST_COMMON=Makefile.in $(CONFFILE_SRC) $(PERL_SCRIPTS_SRC) tsung.sh.in tsung.spec.in


.PHONY: doc

tsung: Makefile config.status $(PERL_SCRIPTS) tsung.sh tsung.spec $(TARGET) $(RECORDER_TARGET) $(CONTROLLER_TARGET) $(LIB_TARGET)

all: clean tsung

debug:
	$(MAKE) TYPE=debug

native:
	$(MAKE) TYPE=native

rpm:	release tsung.spec
	rpmbuild -ta $(distdir).tar.gz

validate: $(CONFFILE)
	@for i in $(CONFFILE); do xmlproc_val $$i; done

deb:
	fakeroot debian/rules clean
	debian/rules build
	fakeroot debian/rules binary

clean:
	-cd priv && rm -f $(shell ls priv | grep -v builder\.erl| grep -v CVS) && cd ..
	-rm -f $(TARGET) $(TMP) $(BUILD_OPTIONS_FILE) builder.beam
	-rm -f $(TGT_APPFILES) tsung.sh $(PERL_SCRIPTS) $(CONFFILE)
	-rm -f ebin/*.beam tsung.sh tsung.spec tsung.xml

install: doc boot install_recorder install_controller $(CONFFILE)
	-rm -f $(TMP)

	install -d $(TARGETDIR)/priv
	install -d $(TARGETDIR)/ebin
	install -d $(TARGETDIR)/src
	install -d $(TARGETDIR)/include
	install -d $(LIBDIR)/
	install -d $(BINDIR)/

	cp $(INC_FILES) $(TARGETDIR)/include
	cp $(TARGET) $(TARGETDIR)/ebin
	cp $(LIB_TARGET) $(TARGETDIR)/ebin
	cp builder.beam $(TARGETDIR)/ebin

	cp $(TGT_APPFILES_E) $(TARGETDIR)/ebin
	cp $(TGT_APPFILES_P) $(TARGETDIR)/priv

	cp $(SRC) $(SRC_APPFILES) $(TARGETDIR)/src
	echo $(BUILD_OPTIONS_DOT) > $(TARGETDIR)/BUILD_OPTIONS

# install the man page & user's manual
	install -d $(MAN_DIR)
	install doc/tsung.1 $(MAN_DIR)
	install -d $(DOC_DIR)/images
	install $(USERMANUAL) $(DOC_DIR)
	install $(USERMANUAL_IMG) $(DOC_DIR)/images

# create startup script
	install tsung.sh $(SCRIPT)
	install $(PERL_SCRIPTS) $(LIBDIR)/

	install -d $(CONFDIR)
	cp $(CONFFILE) $(CONFDIR)

	install -d $(TEMPLATES_DIR)
	cp $(TEMPLATES) $(TEMPLATES_DIR)
	cp $(DTD) $(SHARE_DIR)

install_recorder:
	install -d $(RECORDER_TARGETDIR)/priv
	install -d $(RECORDER_TARGETDIR)/ebin
	install -d $(RECORDER_TARGETDIR)/src
	install -d $(RECORDER_TARGETDIR)/include

	cp $(INC_FILES) $(RECORDER_TARGETDIR)/include
	cp $(RECORDER_TARGET) $(RECORDER_TARGETDIR)/ebin

	cp $(RECORDER_TGT_APPFILES_E) $(RECORDER_TARGETDIR)/ebin
	cp $(RECORDER_TGT_APPFILES_P) $(RECORDER_TARGETDIR)/priv

	cp $(RECORDER_SRC) $(RECORDER_SRC_APPFILES) $(RECORDER_TARGETDIR)/src
	@echo $(BUILD_OPTIONS_DOT) > $(RECORDER_TARGETDIR)/BUILD_OPTIONS

install_controller:
	install -d $(CONTROLLER_TARGETDIR)/priv
	install -d $(CONTROLLER_TARGETDIR)/ebin
	install -d $(CONTROLLER_TARGETDIR)/src
	install -d $(CONTROLLER_TARGETDIR)/include
	cp $(INC_FILES) $(CONTROLLER_TARGETDIR)/include
	cp $(CONTROLLER_TARGET) $(CONTROLLER_TARGETDIR)/ebin

	cp $(CONTROLLER_TGT_APPFILES_E) $(CONTROLLER_TARGETDIR)/ebin
	cp $(CONTROLLER_TGT_APPFILES_P) $(CONTROLLER_TARGETDIR)/priv

	cp $(CONTROLLER_SRC) $(CONTROLLER_SRC_APPFILES) $(CONTROLLER_TARGETDIR)/src
	@echo $(BUILD_OPTIONS_DOT) > $(CONTROLLER_TARGETDIR)/BUILD_OPTIONS

uninstall:
	rm -rf $(TARGETDIR) $(SCRIPT)

boot: tsung priv/tsung.boot priv/tsung_recorder.boot priv/tsung_controller.boot

priv/tsung.boot: builder.beam  $(SRC_APPFILES)
# use builder to make boot file
	@rm -rf temp
	@mkdir -p temp/lib/$(APPLICATION)-$(VERSION)
	@ln -sf $(PWD)/ebin temp/lib/$(APPLICATION)-$(VERSION)/ebin
	@ln -sf $(PWD)/src/$(APPLICATION) temp/lib/$(APPLICATION)-$(VERSION)/src
	@ln -sf $(PWD)/include temp/lib/$(APPLICATION)-$(VERSION)/include
	@ln -sf $(PWD)/priv temp/lib/$(APPLICATION)-$(VERSION)/priv
	@ln -sf $(PWD)/builder.beam temp/lib/$(APPLICATION)-$(VERSION)/
	@ln -sf $(PWD) temp/lib/$(APPLICATION)-$(VERSION)
	@echo -n "build main app boot script ... "
	@(cd temp/lib/$(APPLICATION)-$(VERSION) \
	 && echo $(BUILD_OPTIONS) > $(BUILD_OPTIONS_FILE) \
	 && $(ERL) -noshell -s builder go -s init stop > /dev/null 2>&1 \
	)
	@rm -rf temp
	@echo "done"

priv/tsung_controller.boot: builder.beam $(CONTROLLER_SRC_APPFILES)
# use builder to make boot file
	@rm -rf temp
	@mkdir -p temp/lib/$(CONTROLLER_APPLICATION)-$(VERSION)
	@ln -sf $(PWD)/ebin temp/lib/$(CONTROLLER_APPLICATION)-$(VERSION)/ebin
	@ln -sf $(PWD)/src/$(CONTROLLER_APPLICATION) temp/lib/$(CONTROLLER_APPLICATION)-$(VERSION)/src
	@ln -sf $(PWD)/include temp/lib/$(CONTROLLER_APPLICATION)-$(VERSION)/include
	@ln -sf $(PWD)/priv temp/lib/$(CONTROLLER_APPLICATION)-$(VERSION)/priv
	@ln -sf $(PWD)/builder.beam temp/lib/$(CONTROLLER_APPLICATION)-$(VERSION)/
	@echo -n "build controller boot script ... "
	@(cd temp/lib/$(CONTROLLER_APPLICATION)-$(VERSION) \
	 && echo $(BUILD_OPTIONS) > $(BUILD_OPTIONS_FILE) \
	 && $(ERL) -noshell -s builder go -s init stop  > /dev/null 2>&1 \
	)
	@rm -rf temp
	@echo "done"

priv/tsung_recorder.boot: builder.beam $(RECORDER_SRC_APPFILES)
# use builder to make boot file
	@rm -rf temp
	@mkdir -p temp/lib/$(RECORDER_APPLICATION)-$(VERSION)
	@ln -sf $(PWD)/ebin temp/lib/$(RECORDER_APPLICATION)-$(VERSION)/ebin
	@ln -sf $(PWD)/src/$(RECORDER_APPLICATION) temp/lib/$(RECORDER_APPLICATION)-$(VERSION)/src
	@ln -sf $(PWD)/include temp/lib/$(RECORDER_APPLICATION)-$(VERSION)/include
	@ln -sf $(PWD)/priv temp/lib/$(RECORDER_APPLICATION)-$(VERSION)/priv
	@ln -sf $(PWD)/builder.beam temp/lib/$(RECORDER_APPLICATION)-$(VERSION)/
	@echo -n "build recorder boot script ... "
	@(cd temp/lib/$(RECORDER_APPLICATION)-$(VERSION) \
	 && echo $(BUILD_OPTIONS) > $(BUILD_OPTIONS_FILE) \
	 && $(ERL) -noshell -s builder go -s init stop  > /dev/null 2>&1 \
	)
	@rm -rf temp
	@echo "done"


Makefile: Makefile.in config.status
	@$(SHELL) ./config.status --file=$@

%.pl:  %.pl.in vsn.mk
	@$(SHELL) ./config.status --file=$@

%.spec:  %.spec.in vsn.mk
	@$(SHELL) ./config.status --file=$@

%.xml:  %.xml.in
	@$(SHELL) ./config.status --file=$@

%.sh :%.sh.in vsn.mk
	@$(SHELL) ./config.status --file=$@

config.status: configure $(CONFIG_STATUS_DEPENDENCIES)
	$(SHELL) ./config.status --recheck

configure: configure.in $(CONFIGURE_DEPENDENCIES)
	@echo "running autoconf"
	@autoconf

doc: 
	$(MAKE) -C doc

release: Makefile tsung.spec doc
	rm -fr $(distdir)
	mkdir -p $(distdir)
	tar zcf tmp.tgz $(SRC) $(SRC_APPFILES) $(INC_FILES) $(LIBSRC) \
		$(CONTROLLER_SRC) $(CONTROLLER_SRC_APPFILES) \
		$(RECORDER_SRC) $(RECORDER_SRC_APPFILES) $(TEMPLATES) \
		doc/*.erl doc/*.txt doc/*.fig doc/*.png doc/Makefile doc/*.sgml doc/*.1 \
		$(USERMANUAL) $(USERMANUAL_SRC) $(USERMANUAL_IMG) $(DTD) \
		COPYING README LISEZMOI TODO $(CONFFILE_SRC) \
		priv/builder.erl tsung.sh.in vsn.mk \
		$(DEBIAN)  $(PERL_SCRIPTS_SRC) CONTRIBUTORS CHANGES \
		configure configure.in config.guess config.sub Makefile.in \
		install-sh tsung.spec.in tsung.spec
	tar -C $(distdir) -zxf tmp.tgz
	mkdir $(distdir)/ebin
	tar zvcf  $(distdir).tar.gz $(distdir)
	rm -fr $(distdir)
	rm -fr tmp.tgz

snapshot:
	$(MAKE) TYPE=snapshot release

builder.beam: priv/builder.erl 
	@$(CC) -W0 $(OPT) -I $(INC) $<

ebin/%.beam: src/lib/%.erl $(INC_FILES)
	@echo  "Compiling  $< ... "
	@$(CC) -W0 $(OPT) -I $(INC) -I $(ERLANG_XMERL_DIR) -o ebin $<

ebin/%.beam: src/$(APPLICATION)/%.erl $(INC_FILES)
	@echo  "Compiling  $< ... "
	@$(CC) $(OPT) -I $(INC) -I $(ERLANG_XMERL_DIR) -o ebin $<

ebin/%.beam: src/$(RECORDER_APPLICATION)/%.erl  $(INC_FILES)
	@echo  "Compiling  $< ... "
	@$(CC) $(OPT) -I $(INC) -I $(ERLANG_XMERL_DIR) -o ebin $<

ebin/%.beam: src/$(CONTROLLER_APPLICATION)/%.erl  $(INC_FILES)
	@echo "Compiling  $< ... "
	@$(CC) $(OPT) -I $(INC) -I $(ERLANG_XMERL_DIR) -o ebin $<

%:%.sh
# Override makefile default implicit rule
